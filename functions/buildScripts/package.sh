#!/bin/bash -e
out=../dist
rm -r $out/* || mkdir $out || true
cp -vaR ./config $out/config
cp -vaR ./dist $out/dist
cp -aR ./node_modules $out/node_modules
cp -v ./package.json $out/package.json
cp -v ./package-lock.json $out/package-lock.json
cp -v ./tsconfig.json $out/tsconfig.json
cp -v ./tslint.json $out/tslint.json
exit 0