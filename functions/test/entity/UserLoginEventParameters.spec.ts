import { assert } from 'chai';
import { suite, test} from "mocha-typescript";
import {UserLoginEventParameters} from "../../src/entity/UserLoginEventParameters";
import {EventType} from "../../src/entity/EventType";
import {MessageQueueTopic} from "../../src/entity/MessageQueueTopic";

@suite class UserLoginEventParametersTest {

	private readonly eventID = "test event id";
	private timestampLowerBound;
	private timestampUpperBound;

	private readonly uid = "test uid";
	private readonly userDisplayName = "test user display name";

	private userLoginParameters: UserLoginEventParameters;

	// noinspection JSUnusedGlobalSymbols
	before(){

		this.timestampLowerBound = Date.now();
		this.userLoginParameters = new UserLoginEventParameters(
			this.uid,
			this.userDisplayName
		);
		this.timestampUpperBound = Date.now();
	}

	@test
	given_parameters_when_constructed_should_populate_fields(){

		const parameters = this.userLoginParameters;

		assert.equal(parameters.messageQueueTopic, MessageQueueTopic.AUTHENTICATION, "message queue topic not equal");
		assert.equal(parameters.type, EventType.USER_LOGIN_EVENT, "type is not equal");
		assert.isAtLeast(parameters.timestampMillis, this.timestampLowerBound, "timestamp not equal");
		assert.isAtMost(parameters.timestampMillis, this.timestampUpperBound, "timestamp not equal");
		assert.deepEqual(parameters.data, {uid: this.uid, userDisplayName: this.userDisplayName}, "data not equal");
	}

	@test
	given_parameters_when_to_event_should_create_event(){

		const event = this.userLoginParameters.toEvent(this.eventID);

		assert.equal(event.eventID, this.eventID, "event id not equal");
		assert.equal(event.messageQueueTopic, MessageQueueTopic.AUTHENTICATION, "message queue topic not equal");
		assert.equal(event.type, EventType.USER_LOGIN_EVENT, "type is not equal");
		assert.isAtLeast(event.timestampMillis, this.timestampLowerBound, "timestamp not equal");
		assert.isAtMost(event.timestampMillis, this.timestampUpperBound, "timestamp not equal");
		assert.deepEqual(event.data, {uid: this.uid, userDisplayName: this.userDisplayName}, "data not equal");
	}
}