import { assert } from 'chai';
import { suite, test} from "mocha-typescript";
import {EventType} from "../../src/entity/EventType";
import {MessageQueueTopic} from "../../src/entity/MessageQueueTopic";
import {EventPayload, Event} from "../../src/entity/Event";

@suite class EventTest {

	private readonly eventID = "test event id";
	private readonly messageTopic = MessageQueueTopic.AUTHENTICATION;
	private readonly eventType = EventType.USER_LOGIN_EVENT;
	private readonly timestampMillis = 1234;
	private readonly eventData = {test1: "test", test2: "test"};

	private event;

	// noinspection JSUnusedGlobalSymbols
	before(){

		this.event = new Event(
			this.eventID,
			this.messageTopic,
			this.eventType,
			this.timestampMillis,
			this.eventData
		);
	}

	@test
	given_parameters_when_constructed_should_populate_fields(){

		const event: Event = this.event;

		assert.equal(event.eventID, this.eventID, "message queue topic not equal");
		assert.equal(event.messageQueueTopic, MessageQueueTopic.AUTHENTICATION, "message queue topic not equal");
		assert.equal(event.type, EventType.USER_LOGIN_EVENT, "type is not equal");
		assert.equal(event.timestampMillis, this.timestampMillis, "timestamp not equal");
		assert.deepEqual(event.data, this.eventData, "data not equal");
	}

	@test
	given_parameters_when_to_payload_should_create_payload(){

		const payload: EventPayload = this.event.toPayload();

		assert.equal(payload.eventID, this.eventID, "message queue topic not equal");
		assert.equal(payload.type, EventType.USER_LOGIN_EVENT, "type is not equal");
		assert.equal(payload.timestampMillis, this.timestampMillis, "timestamp not equal");
		assert.deepEqual(payload.data, this.eventData, "data not equal");
	}

}