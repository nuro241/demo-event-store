import {suite, test} from "mocha-typescript";
import {ExpressUtilities as Util} from "./ExpressUtilities";
import {Router} from "express";
import {InteractorFactory} from "../../src/controller/Controller";
import {AddEventInteractor} from "../../src/interactor/AddEventInteractor";
import {instance, mock, reset, spy, verify, when} from "ts-mockito";
import {EventParameters} from "../../src/entity/EventParameters";
import {UserLoginController, UserLoginEventParametersFactory} from "../../src/controller/UserLoginController";
import {BAD_REQUEST, OK, INTERNAL_SERVER_ERROR, NOT_FOUND} from 'http-status-codes';
import {UserLoginEventParameters} from "../../src/entity/UserLoginEventParameters";

@suite
export class UserLoginControllerTest{

	private interactor: AddEventInteractor;
	private interactorFactory: InteractorFactory;
	private eventParameters: UserLoginEventParameters;
	private eventParametersFactory: UserLoginEventParametersFactory;

	private router: Router;

	// noinspection JSUnusedGlobalSymbols
	before(){

		this.eventParameters = mock(UserLoginEventParameters);
		this.eventParametersFactory = spy({create: (uid: string, userDisplayName: string) => {return instance(this.eventParameters)}});

		this.interactor = mock(AddEventInteractor);
		when(this.interactor.execute()).thenReturn(Promise.resolve());
		this.interactorFactory = spy({create : (parameters: EventParameters) => {return instance(this.interactor)}});

		this.router = new UserLoginController(instance(this.eventParametersFactory), instance(this.interactorFactory)).getRouter();
	}

	@test
	async given_valid_parameters_and_resolving_interactor_should_call_interactor_with_parameters_and_return_ok(){

		const url = "testUrl";
		const method = "PUT";
		const body = {uid: 'test uid', userDisplayName: 'test user display name'};
		const req = Util.mockRequest({url, method, body});

		const res = await Util.callEndpoint(this.router, url, req);

		verify(this.eventParametersFactory.create(body.uid, body.userDisplayName)).times(1);
		verify(this.interactorFactory.create(instance(this.eventParameters))).times(1);
		verify(this.interactor.execute()).times(1);
		Util.assertStatus(res, OK);
	}

	@test
	async given_valid_parameters_and_rejecting_interactor_should_call_interactor_with_parameters_and_return_internal_server_error(){

		const url = "testUrl";
		const method = "PUT";
		const body = {uid: 'test uid', userDisplayName: 'test user display name'};
		const req = Util.mockRequest({url, method, body});
		const error = new Error("test error message");
		reset(this.interactor);
		when(this.interactor.execute()).thenReturn(Promise.reject(error));

		const res = await Util.callEndpoint(this.router, url, req);

		Util.assertStatus(res, INTERNAL_SERVER_ERROR);
	}

	@test
	async given_invalid_parameters_should_return_bad_request(){

		const url = "testUrl";
		const method = "PUT";
		const body = {invalid1: 'test uid', invalid2: 'test user display name'};
		const req = Util.mockRequest({url, method, body});

		const res = await Util.callEndpoint(this.router, url, req);

		Util.assertStatus(res, BAD_REQUEST);
	}

	@test
	async given_invalid_http_method_should_throw_error(){

		const url = "testUrl";
		const method = "GET";
		const body = {uid: 'test uid', userDisplayName: 'test user display name'};
		const req = Util.mockRequest({url, method, body});

		const res = await Util.callEndpoint(this.router, url, req);

		Util.assertStatus(res, NOT_FOUND);
	}
}