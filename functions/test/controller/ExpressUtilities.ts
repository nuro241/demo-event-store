import * as HttpMock from 'node-mocks-http';
import * as Express from "express";
import {EventEmitter} from 'events';
import {getStatusText} from "http-status-codes";
import {assert} from "chai";

export class ExpressUtilities{

	public static mockRequest(options: HttpMock.RequestOptions): Express.Request {

		if(!options.url)
			throw new Error("url should not be empty or undefined");

		return HttpMock.createRequest(options);
	}

	public static callEndpoint(router: Express.Router, pathParams: string, request: Express.Request): Promise<Express.Response>{

		if(!pathParams)
			throw new Error("path parameters should not be empty or undefined");

		return new Promise((resolve, reject) => {

			const response = HttpMock.createResponse({
				eventEmitter: EventEmitter
			});

			response.on('end', () => {
				try {
					response['body'] = JSON.parse(response._getData());
				} catch (error) {
					response['body'] = response._getData();
				}
			});

			response.on('end', () => {resolve(response)});

			request['resume'] = () => {return request};

			try{
				const app = Express();
				app.use(pathParams, router);
				app(request, response);
			}catch (error){
				reject(error);
			}

		});

	};

	static assertStatus(res: Express.Response, statusCode: number) {

		assert.equal(res.statusCode, statusCode, "status code not equal");
		assert.equal(res.statusMessage, getStatusText(statusCode), "status message not equal");
	}
}