import {MessageBroker} from "../../src/interactor/MessageBroker";
import {Event} from "../../src/entity/Event";

export class MockMessageBroker implements MessageBroker{

	send(event: Event): Promise<void> {
		throw new Error("method not implemented");
	}
}