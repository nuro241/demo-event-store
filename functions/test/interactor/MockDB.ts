import {DB} from "../../src/interactor/DB";
import {EventParameters} from "../../src/entity/EventParameters";
import {Event} from "../../src/entity/Event";

export class MockDB implements DB{

	addEvent(parameters: EventParameters): Promise<Event> {
		throw new Error("method not implemented");
	}

	removeEvent(event: Event): Promise<void> {
		throw new Error("method not implemented");
	}
}