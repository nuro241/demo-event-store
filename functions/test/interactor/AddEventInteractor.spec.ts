import { suite, test} from "mocha-typescript";
import {instance, mock, verify, when, anything} from "ts-mockito";
import {MockMessageBroker} from "./MockMessageBroker";
import {MockDB} from "./MockDB";
import {assert} from "chai";
import {fail} from "assert";
import {DB} from "../../src/interactor/DB";
import {MessageBroker} from "../../src/interactor/MessageBroker";
import {AddEventInteractor} from "../../src/interactor/AddEventInteractor";
import {EventParameters} from "../../src/entity/EventParameters";
import {Event} from "../../src/entity/Event";


@suite class AddEventInteractorTest {

	private eventParameters: EventParameters;
	private db: DB;
	private messageBroker: MessageBroker;

	private interactor: AddEventInteractor;

	// noinspection JSUnusedGlobalSymbols
	before(){

		this.eventParameters = instance(mock(EventParameters));
		this.db = mock(MockDB);
		this.messageBroker = mock(MockMessageBroker);

		this.interactor = new AddEventInteractor(
			this.eventParameters,
			instance(this.db),
			instance(this.messageBroker)
		);
	}

	@test
	async when_execute_and_add_to_db_and_messaging_notification_is_successful_should_save_to_db_and_notify_listeners_and_resolve(){

		const event = instance(mock(Event));
		when(this.db.addEvent(this.eventParameters)).thenReturn(Promise.resolve(event));
		when(this.messageBroker.send(event)).thenReturn(Promise.resolve());

		await this.interactor.execute();

		//verify(this.db.addEvent(this.eventParameters)).times(1);
		verify(this.messageBroker.send(event)).times(1);
	}

	@test
	async when_execute_and_add_to_db_is_unsuccessful_should_return_error_and_not_notify_listeners_and_not_try_to_delete_event(){

		const mockError = new Error('test error message');
		when(this.db.addEvent(this.eventParameters)).thenReturn(Promise.reject(mockError));

		try{
			await this.interactor.execute();
			fail("", "", "did not return error");
		}catch (error){

			assert.equal(error, mockError, "error not equal");
			verify(this.messageBroker.send(anything())).never();
			verify(this.db.removeEvent(anything())).never();
		}
	}

	@test
	async when_execute_and_add_to_db_is_successful_and_notify_listeners_is_unsuccessful_and_remove_from_db_is_successful_should_return_error(){

		const event = instance(mock(Event));
		const mockError = new Error('test error message');
		when(this.db.addEvent(this.eventParameters)).thenReturn(Promise.resolve(event));
		when(this.messageBroker.send(event)).thenReturn(Promise.reject(mockError));
		when(this.db.removeEvent(event)).thenReturn(Promise.resolve());

		try{
			await this.interactor.execute();
			fail("", "", "did not return error");
		}catch (error){

			assert.equal(error, mockError, "error not equal");
			verify(this.db.addEvent(this.eventParameters)).times(1);
			verify(this.messageBroker.send(event)).times(1);
			verify(this.db.removeEvent(event)).times(1);
		}
	}

	@test
	async when_execute_and_add_to_db_is_successful_and_notify_listeners_is_unsuccessful_and_remove_from_db_is_unsuccessful_should_return_error(){

		const event = instance(mock(Event));
		const mockError = new Error('test error message');
		when(this.db.addEvent(this.eventParameters)).thenReturn(Promise.resolve(event));
		when(this.messageBroker.send(event)).thenReturn(Promise.reject(""));
		when(this.db.removeEvent(event)).thenReturn(Promise.reject(mockError));

		try{
			await this.interactor.execute();
			fail("", "", "did not return error");
		}catch (error){

			assert.equal(error, mockError, "error not equal");
			verify(this.db.addEvent(this.eventParameters)).times(1);
			verify(this.messageBroker.send(event)).times(1);
			verify(this.db.removeEvent(event)).times(1);
		}
	}
}
