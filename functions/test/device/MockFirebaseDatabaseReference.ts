import * as admin from "firebase-admin";

export class MockFirebaseDatabaseReference implements admin.database.Reference{

	key: string | null;
	// noinspection JSUnusedGlobalSymbols
	parent: admin.database.Reference | null;
	// noinspection JSUnusedGlobalSymbols
	root: admin.database.Reference;
	// noinspection JSUnusedGlobalSymbols
	path: string;
	// noinspection JSUnusedGlobalSymbols
	ref: admin.database.Reference;

	child(path: string): admin.database.Reference {
		throw new Error("method not implemented");
	}

	onDisconnect(): admin.database.OnDisconnect {
		throw new Error("method not implemented");
	}

	push(value?: any, onComplete?: (a: (Error | null)) => any): admin.database.ThenableReference {
		throw new Error("method not implemented");
	}

	remove(onComplete?: (a: (Error | null)) => any): Promise<void> {
		throw new Error("method not implemented");
	}

	set(value: any, onComplete?: (a: (Error | null)) => any): Promise<void> {
		throw new Error("method not implemented");
	}

	setPriority(priority: string | number | null, onComplete: (a: (Error | null)) => any): Promise<void> {
		throw new Error("method not implemented");
	}

	setWithPriority(newVal: any, newPriority: string | number | null, onComplete?: (a: (Error | null)) => any): Promise<void> {
		throw new Error("method not implemented");
	}

	transaction(transactionUpdate: (a: any) => any, onComplete?: (a: (Error | null), b: boolean, c: (admin.database.DataSnapshot | null)) => any, applyLocally?: boolean): Promise<{ committed: boolean; snapshot: admin.database.DataSnapshot | null }> {
		throw new Error("method not implemented");
	}

	update(values: Object, onComplete?: (a: (Error | null)) => any): Promise<void> {
		throw new Error("method not implemented");
	}

	endAt(value: number | string | boolean | null, key?: string): admin.database.Query {
		throw new Error("method not implemented");
	}

	equalTo(value: number | string | boolean | null, key?: string): admin.database.Query {
		throw new Error("method not implemented");
	}

	isEqual(other: admin.database.Query | null): boolean {
		throw new Error("method not implemented");
	}

	limitToFirst(limit: number): admin.database.Query {
		throw new Error("method not implemented");
	}

	limitToLast(limit: number): admin.database.Query {
		throw new Error("method not implemented");
	}

	off(eventType?: admin.database.EventType, callback?: (a: admin.database.DataSnapshot, b?: (string | null)) => any, context?: Object | null): void {
		throw new Error("method not implemented");
	}

	on(eventType: admin.database.EventType, callback: (a: (admin.database.DataSnapshot | null), b?: string) => any, cancelCallbackOrContext?: Object | null, context?: Object | null): (a: (admin.database.DataSnapshot | null), b?: string) => any {
		throw new Error("method not implemented");
	}

	once(eventType: admin.database.EventType, successCallback?: (a: admin.database.DataSnapshot, b?: string) => any, failureCallbackOrContext?: Object | null, context?: Object | null): Promise<any> {
		throw new Error("method not implemented");
	}

	orderByChild(path: string): admin.database.Query {
		throw new Error("method not implemented");
	}

	orderByKey(): admin.database.Query {
		throw new Error("method not implemented");
	}

	orderByPriority(): admin.database.Query {
		throw new Error("method not implemented");
	}

	orderByValue(): admin.database.Query {
		throw new Error("method not implemented");
	}

	startAt(value: number | string | boolean | null, key?: string): admin.database.Query {
		throw new Error("method not implemented");
	}

	toJSON(): Object {
		throw new Error("method not implemented");
	}

}