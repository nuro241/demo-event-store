import {UserLoginEventParameters} from "../../src/entity/UserLoginEventParameters";
import {anything, spy, verify, when} from "ts-mockito";
import {suite, test} from "mocha-typescript";
import {FirebaseDB} from "../../src/device/FirebaseDB";
import * as admin from "firebase-admin";
import {MockFirebaseDatabaseReference} from "./MockFirebaseDatabaseReference";
import {MockFirebaseDatabase} from "./MockFirebaseDatabase";

@suite
export class FirebaseDBTest {

	private mockDB: admin.database.Database;

	private firebaseDB: FirebaseDB;

	// noinspection JSUnusedGlobalSymbols
	before() {

		const db = new MockFirebaseDatabase();
		this.mockDB = spy(db);

		this.firebaseDB = new FirebaseDB(db);
	}

	@test
	async given_event_parameters_when_add_event_should_add_event_to_db_at_correct_location(){

		const eventID = "test event id";
		const eventParameters = new UserLoginEventParameters("test uid", "test display name");
		const mockEventParameters = spy(eventParameters);

		const reference = new MockFirebaseDatabaseReference();
		const mockReference = spy(reference);
		when(this.mockDB.ref(anything())).thenReturn(reference);
		when(mockReference.push()).thenCall(() => {
			reference.key = eventID;
			return reference;
		});
		when(mockReference.set(anything())).thenReturn(Promise.resolve());

		await this.firebaseDB.addEvent(eventParameters);

		verify(this.mockDB.ref(eventParameters.messageQueueTopic)).times(1);
		verify(mockReference.push()).times(1);
		verify(mockEventParameters.toEvent(eventID));
		verify(mockReference.set(eventParameters.toEvent(eventID).toPayload()));
	}

	@test
	async given_event_when_remove_event_should_remove_event_from_db(){

		const event = new UserLoginEventParameters("test uid", "test display name")
			.toEvent("test event id");
		const reference = new MockFirebaseDatabaseReference();
		const mockReference = spy(reference);
		when(this.mockDB.ref(anything())).thenReturn(reference);
		when(mockReference.set(anything())).thenReturn(Promise.resolve());

		await this.firebaseDB.removeEvent(event);

		verify(this.mockDB.ref(event.messageQueueTopic + "/" + event.eventID)).times(1);
		verify(mockReference.set(null));
	}
}