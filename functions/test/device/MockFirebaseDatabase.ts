import * as admin from "firebase-admin";

export class MockFirebaseDatabase implements admin.database.Database{

	// noinspection JSUnusedGlobalSymbols
	app: admin.app.App;

	goOffline(): void {
		throw new Error("method not implemented");
	}

	goOnline(): void {
		throw new Error("method not implemented");
	}

	ref(path?: string): admin.database.Reference {
		throw new Error("method not implemented");
	}

	refFromURL(url: string): admin.database.Reference {
		throw new Error("method not implemented");
	}
}
