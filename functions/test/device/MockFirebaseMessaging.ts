import * as admin from "firebase-admin";

export class MockFirebaseMessaging implements admin.messaging.Messaging{

	// noinspection JSUnusedGlobalSymbols
	app: admin.app.App;

	sendToDevice(registrationToken: string | string[], payload: admin.messaging.MessagingPayload, options?: admin.messaging.MessagingOptions): Promise<admin.messaging.MessagingDevicesResponse> {
		throw new Error("method not implemented");
	}

	sendToDeviceGroup(notificationKey: string, payload: admin.messaging.MessagingPayload, options?: admin.messaging.MessagingOptions): Promise<admin.messaging.MessagingDeviceGroupResponse> {
		throw new Error("method not implemented");
	}

	sendToTopic(topic: string, payload: admin.messaging.MessagingPayload, options?: admin.messaging.MessagingOptions): Promise<admin.messaging.MessagingTopicResponse> {
		throw new Error("method not implemented");
	}

	sendToCondition(condition: string, payload: admin.messaging.MessagingPayload, options?: admin.messaging.MessagingOptions): Promise<admin.messaging.MessagingConditionResponse> {
		throw new Error("method not implemented");
	}

	subscribeToTopic(registrationToken: string | string[], topic: string): Promise<admin.messaging.MessagingTopicManagementResponse>;
	subscribeToTopic(registrationToken, topic: string): Promise<admin.messaging.MessagingTopicManagementResponse> {
		throw new Error("method not implemented");
	}

	unsubscribeFromTopic(registrationToken: string | string[], topic: string): Promise<admin.messaging.MessagingTopicManagementResponse>;
	unsubscribeFromTopic(registrationToken, topic: string): Promise<admin.messaging.MessagingTopicManagementResponse> {
		throw new Error("method not implemented");
	}

}