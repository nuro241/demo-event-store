import {UserLoginEventParameters} from "../../src/entity/UserLoginEventParameters";
import {anything, capture, spy, verify, when} from "ts-mockito";
import {suite, test} from "mocha-typescript";
import * as admin from "firebase-admin";
import {FirebaseMessageBroker} from "../../src/device/FirebaseMessageBroker";
import {EventPayload} from "../../src/entity/Event";
import {assert} from 'chai';
import {MockFirebaseMessaging} from "./MockFirebaseMessaging";

@suite
export class FirebaseMessageBrokerTest {

	private mockMessaging: admin.messaging.Messaging;

	private firebaseMessageBroker: FirebaseMessageBroker;

	// noinspection JSUnusedGlobalSymbols
	before() {

		const messaging = new MockFirebaseMessaging();
		this.mockMessaging = spy(messaging);

		this.firebaseMessageBroker = new FirebaseMessageBroker(messaging);
	}

	@test
	async given_event_when_send_should_send_to_topic(){

		const event = new UserLoginEventParameters("test uid", "test display name")
			.toEvent("test event id");
		when(this.mockMessaging.sendToTopic(anything(), anything())).thenReturn(Promise.reject(""));

		try {
			await this.firebaseMessageBroker.send(event);
		}catch (error){}

		const [actualTopic, actualPayload] = capture(this.mockMessaging.sendToTopic).first();
		assert.strictEqual(actualTopic, event.messageQueueTopic, "message topic not equal");
		assert.deepEqual(actualPayload, FirebaseMessageBrokerTest.toMessagingPayload(event.toPayload()), "event payload not equal");
		verify(this.mockMessaging.sendToTopic(anything(), anything())).times(1);
	}

	private static toMessagingPayload(eventPayload: EventPayload): admin.messaging.MessagingPayload {

		const payload: admin.messaging.MessagingPayload = {data: {}};

		if(payload.data){
			for (const key in eventPayload) {
				if (eventPayload.hasOwnProperty(key)) {
					payload.data[key] = String(eventPayload[key]);
				}
			}
		}

		return payload;
	}
}