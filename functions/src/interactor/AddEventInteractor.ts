import {EventParameters} from "../entity/EventParameters";
import {DB} from "./DB";
import {MessageBroker} from "./MessageBroker";
import {Event} from "../entity/Event";

export class AddEventInteractor{

	constructor(
		private readonly eventParameters: EventParameters,
		private readonly db: DB,
		private readonly messageBroker: MessageBroker
	){}

	async execute(): Promise<void>{

		const event = await this.addEventToDB();
		await this.notifyListeners(event);
	}

	private async addEventToDB(): Promise<Event>{

		try {
			return await this.db.addEvent(this.eventParameters);
		} catch (error) {
			console.error(`failed to add event data to db: ${JSON.stringify(this.eventParameters)}`, error);
			throw error;
		}
	}

	private async notifyListeners(event: Event): Promise<void>{

		try {
			await this.messageBroker.send(event);
		} catch (error) {
			const errorReason = `failed to notify listeners of event ${JSON.stringify(event)}`;
			console.error(errorReason, error);
			await this.removeEventOnMessageNotifyError(event);
			throw error;
		}
	}

	private async removeEventOnMessageNotifyError(event: Event): Promise<void>{

		try {
			await this.db.removeEvent(event);
		} catch (error) {
			console.error(`failed to reverse event in db after failure to notify listeners: ${JSON.stringify(event)}`, error);
			throw error;
		}
	}
}


