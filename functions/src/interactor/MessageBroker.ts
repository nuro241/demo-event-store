import {Event} from "../entity/Event";

export interface MessageBroker{

	send(event: Event): Promise<void>;
}