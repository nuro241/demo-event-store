import {EventParameters} from "../entity/EventParameters";
import {Event} from "../entity/Event";

export interface DB{

	addEvent(parameters: EventParameters): Promise<Event>;
	removeEvent(event: Event): Promise<void>;
}