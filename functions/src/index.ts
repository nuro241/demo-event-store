import * as functions from 'firebase-functions';
import * as admin from "firebase-admin";
import {FirebaseMessageBroker} from "./device/FirebaseMessageBroker";
import {FirebaseDB} from "./device/FirebaseDB";
import * as Express from 'express';
import {RouteMapper} from "./device/RouteMapper";
import {EventParameters} from "./entity/EventParameters";
import {UserLoginController, UserLoginEventParametersFactory} from "./controller/UserLoginController";
import {AddEventInteractor} from "./interactor/AddEventInteractor";
import {UserLoginEventParameters} from "./entity/UserLoginEventParameters";
import {InteractorFactory} from "./controller/Controller";

const serviceAccount = require('../config/firebase_config_dev.json');
admin.initializeApp({
	credential: admin.credential.cert(serviceAccount),
	databaseURL: "https://kage-bunshin-event-store.firebaseio.com"
});

const firebaseDB = new FirebaseDB(admin.database());
const firebaseMessaging = new FirebaseMessageBroker(admin.messaging());

const interactorFactory: InteractorFactory = {
	create: (eventParameters: EventParameters) => {return new AddEventInteractor(eventParameters, firebaseDB, firebaseMessaging)}
};

const userLoginEventParameters: UserLoginEventParametersFactory = {
	create: (uid: string, userDisplayName: string) => {return new UserLoginEventParameters(uid, userDisplayName)}
};
const userLoginController: UserLoginController = new UserLoginController(userLoginEventParameters, interactorFactory);

const app = Express();
new RouteMapper(app).map(userLoginController);

export const event = functions.https.onRequest(app);