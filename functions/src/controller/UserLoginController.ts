import {Request, Response, NextFunction, Router} from "express";
import {InteractorFactory, Controller} from "./Controller";
import {UserLoginEventParameters} from "../entity/UserLoginEventParameters";
import {BAD_REQUEST, OK, INTERNAL_SERVER_ERROR, getStatusText, NOT_FOUND} from 'http-status-codes';

export interface UserLoginEventParametersFactory{

	create(uid: string, userDisplayName: string): UserLoginEventParameters;
}

export class UserLoginController extends Controller{

	private readonly router: Router;
	private params: UserLoginEventParameters;

	constructor(
		private readonly eventParametersFactory: UserLoginEventParametersFactory,
		interactorFactory: InteractorFactory,
	){
		super(interactorFactory);

		this.router = Router();

		this.router.put(Controller.ROOT_PATH, this.authCallback);
		this.router.put(Controller.ROOT_PATH, this.extractParametersCallback);
		this.router.put(Controller.ROOT_PATH, this.executeCallback);
		this.router.use(Controller.ROOT_PATH, this.invalidHttpMethodCallback);
	}

	getRouter(): Router {

		return this.router;
	}

	private readonly authCallback = (req: Request, res: Response, next: NextFunction) => {

		next();
	};

	private readonly extractParametersCallback = (req: Request, res: Response, next: NextFunction) => {

		const uid: string = req.body.uid;
		const userDisplayName: string = req.body.userDisplayName;

		if(uid && userDisplayName){

			this.params = this.eventParametersFactory.create(uid, userDisplayName);
			next();

		}else{

			console.log(`invalid parameters in request body: uid {${uid}}, userDisplayName {${userDisplayName}}`);
			UserLoginController.setResponseStatus(res, BAD_REQUEST);
		}
	};

	private readonly executeCallback = (req: Request, res: Response) => {

		this.interactorFactory.create(this.params).execute().then(

			() => {

				UserLoginController.setResponseStatus(res, OK);
			},
			(error) => {

				console.error("failed to execute user login interactor", error);
				UserLoginController.setResponseStatus(res, INTERNAL_SERVER_ERROR);
			}
		)
	};

	protected readonly invalidHttpMethodCallback = (req: Request, res: Response, next: NextFunction) => {

		if (!req.route)
			UserLoginController.setResponseStatus(res, NOT_FOUND);

		next();
	};

	private static setResponseStatus(res: Response, statusCode: number) {

		res.statusMessage = getStatusText(statusCode);
		res.status(statusCode).send();
	}
}