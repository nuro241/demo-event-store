import {Router} from "express";
import {AddEventInteractor} from "../interactor/AddEventInteractor";
import {EventParameters} from "../entity/EventParameters";

export interface InteractorFactory{

	create(parameters: EventParameters): AddEventInteractor
}

export abstract class Controller{

	public static readonly ROOT_PATH = "";

	constructor(protected readonly interactorFactory: InteractorFactory){}

	abstract getRouter(): Router;
}