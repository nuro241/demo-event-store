import {Event, EventPayload} from "../entity/Event";
import {MessageBroker} from "../interactor/MessageBroker";
import * as admin from "firebase-admin";
import {toString} from './Util';

export class FirebaseMessageBroker implements MessageBroker{

	constructor(private readonly messaging: admin.messaging.Messaging){}

	send(event: Event): Promise<void> {

		const messagingPayload = FirebaseMessageBroker.toMessagingPayload(event.toPayload());

		return this.messaging.sendToTopic(
			event.messageQueueTopic,
			messagingPayload
		)
			.then(() => {
				console.log(`sent to topic {${event.messageQueueTopic}} message ${toString(messagingPayload)}`);
			});
	}

	public static toMessagingPayload(eventPayload: EventPayload): admin.messaging.MessagingPayload {

		const payload: admin.messaging.MessagingPayload = {data: {}};

		if(payload.data){
			for (const key in eventPayload) {
				if (eventPayload.hasOwnProperty(key)) {
					payload.data[key] = String(eventPayload[key]);
				}
			}
		}

		return payload;
	}
}