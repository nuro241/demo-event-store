import {Event} from "../entity/Event";
import {DB} from "../interactor/DB";
import {EventParameters} from "../entity/EventParameters";
import * as admin from "firebase-admin";
import {toString} from './Util';

export class FirebaseDB implements DB{

	constructor(private readonly db: admin.database.Database){}

	addEvent(parameters: EventParameters): Promise<Event> {

		const newRef = this.db.ref(parameters.messageQueueTopic).push();

		if(newRef.key){

			const event = parameters.toEvent(newRef.key);
			return newRef.set(event.toPayload()).then(() => {
				console.log(`wrote to db, topic {${event.messageQueueTopic}}, event {${toString(event.toPayload())}}`);
				return event
			});

		}else{
			throw new Error("database reference key is null after push");
		}
	}

	removeEvent(event: Event): Promise<void> {

		return this.db.ref(event.messageQueueTopic + "/" + event.eventID).set(null);
	}
}