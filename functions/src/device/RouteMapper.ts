import * as Express from 'express';
import {UserLoginController} from "../controller/UserLoginController";

export class RouteMapper{

	private static USER_LOGIN_1 = "/v1/userLogin";

	constructor(private readonly app: Express.Application){}

	map(userLoginController: UserLoginController){

		this.app.use(RouteMapper.USER_LOGIN_1, userLoginController.getRouter());
	}
}