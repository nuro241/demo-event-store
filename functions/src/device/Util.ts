import {inspect} from "util";

export function toString(a: any): string{
	return inspect(a, false, null);
}