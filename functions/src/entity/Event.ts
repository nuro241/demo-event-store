import {EventType} from "./EventType";
import {EventData} from "./EventParameters";
import {MessageQueueTopic} from "./MessageQueueTopic";

export class Event{

	constructor(
		public readonly eventID: string,
		public readonly messageQueueTopic: MessageQueueTopic,
		public readonly type : EventType,
		public readonly timestampMillis: number,
		public readonly data: EventData
	) {

	}

	public toPayload() : EventPayload {

		return {
			eventID: this.eventID,
			type: this.type,
			timestampMillis: this.timestampMillis,
			data: this.data
		}
	}
}

export interface EventPayload {

	eventID: string,
	type: EventType,
	timestampMillis: number,
	data: EventData
}