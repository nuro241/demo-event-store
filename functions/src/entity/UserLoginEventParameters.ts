import {Event} from "./Event";
import {MessageQueueTopic} from "./MessageQueueTopic";
import {EventType} from "./EventType";
import {EventParameters} from "./EventParameters";

export class UserLoginEventParameters extends EventParameters{

	private static MESSAGE_QUEUE_TOPIC = MessageQueueTopic.AUTHENTICATION;
	private static EVENT_TYPE = EventType.USER_LOGIN_EVENT;

	private readonly eventData;

	constructor(
		uid: string,
		userDisplayName: string
	){

		super(
			UserLoginEventParameters.MESSAGE_QUEUE_TOPIC,
			UserLoginEventParameters.EVENT_TYPE,
			{uid: uid, userDisplayName: userDisplayName}
		);

		this.eventData = {uid, userDisplayName};
	}


	toEvent(eventID: string): Event {

		return new Event(
			eventID,
			UserLoginEventParameters.MESSAGE_QUEUE_TOPIC,
			UserLoginEventParameters.EVENT_TYPE,
			this.timestampMillis,
			this.eventData
		);
	}
}