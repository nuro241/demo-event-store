import {EventType} from "./EventType";
import {MessageQueueTopic} from "./MessageQueueTopic";
import {Event} from "./Event";

export abstract class EventParameters {

	public readonly timestampMillis: number;

	constructor(
		public readonly messageQueueTopic: MessageQueueTopic,
		public readonly type: EventType,
		public readonly data: EventData
	) {

		this.timestampMillis = Date.now();
	}

	abstract toEvent(eventID: string): Event;
}

export interface EventData {

	readonly [key: string]: string | number | EventData
}